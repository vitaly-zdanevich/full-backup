BEFORE=$(df -h)

STARTED=$(date)

DATE=`date "+%Y-%m-%d"`

DEST="/mnt/backup/$DATE"

rsync --archive --acls --xattrs --delete --progress --verbose --exclude-from=exclude.txt --filter=':- .gitignore' --link-dest=/mnt/backup/last --mkpath / $DEST

ln --symbolic --force --no-dereference $DATE /mnt/backup/last

echo "Started at:   " $STARTED
echo "Current time: " $(date)

echo "Before:

$BEFORE

Now:
"

df -h

# How to restore:
#
# rsync --archive --acls --xattrs --progress --verbose <from> <to>
# /etc/fstab: alter UUID
# grub install (if new SSD) https://wiki.gentoo.org/wiki/Grub:
#     grub-install
# Note that if SSD is more than 2 GB - you cannot use MBR, only GPT with EFI partition (Thinkpag T430 supports UEFI boot)
# emerge-webrsync
# emerge --sync guru


# Documentation:
# https://jumpcloud.com/blog/how-to-backup-linux-system-rsync
# https://wiki.archlinux.org/title/rsync
