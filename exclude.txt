/dev/*
/proc/*
/sys/*
/run/*

/var/db/repos/gentoo
/var/db/repos/guru
/var/cache/distfiles/

/tmp/*
/var/tmp

/lost+found
/mnt/*

/home/vitaly/.npm/
/home/vitaly/.cache/

/home/vitaly/.zoom/

/home/vitaly/.mozilla/firefox/*/storage
/home/vitaly/.config/google-chrome/
/home/vitaly/.config/chromium/
/home/vitaly/.config/Microsoft/Microsoft Teams/
/home/vitaly/.config/teams-for-linux/
/home/vitaly/.config/Slack/
/home/vitaly/.thumbnails/

/home/vitaly/.local/share/TelegramDesktop/tdata/user_data/cache/

/home/vitaly/go/pkg/mod/cache

/home/vitaly/record/out
